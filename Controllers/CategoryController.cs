﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Dapper;
using LegalHunter.Data;
using LegalHunter.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using System.IO;
using System.Drawing;
using Microsoft.AspNetCore.Mvc.Rendering;
namespace LegalHunter.Controllers
{
    [Authorize(Roles = "PlatformOwner")]
    public class CategoryController : Controller
    {
        readonly ApplicationDbContext db;
        private readonly IHostingEnvironment _env;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;

        public CategoryController(IHostingEnvironment env, IMapper mapper, ApplicationDbContext Context, UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> SignInManager, IConfiguration configuration)
        {
            _env = env;
            db = Context;
            _userManager = userManager;
            _signInManager = SignInManager;
            _mapper = mapper;
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }
        public IActionResult Index()
        {
            return View();
        }

        #region Category
        public IActionResult Category(long id, string Option)
        {
            //ViewBag.ParentCategoryList = db.CategoryTbl.Where(x => x.IsActive == true && x.ParentCategoryID == 0).ToList();
            var ParentCategoryList = GetCategoryData();
            ViewBag.ParentCategoryList = ParentCategoryList;
            CategoryViewModel CategoryToUpdate = new CategoryViewModel();
            try
            {

                if (Option == "Edit")
                {
                    var Categorydetails = db.CategoryTbl.Where(x=>x.CategoryID==id).FirstOrDefault();
                    CategoryToUpdate = _mapper.Map<CategoryViewModel>(Categorydetails);
                    if (Categorydetails.ParentCategoryID == 0)
                    {
                        CategoryToUpdate.IsParentCategory = true;
                    }
                    else
                    {
                        CategoryToUpdate.IsParentCategory = false;
                    }
                    CategoryToUpdate.Option = "Edit";
                }
                else
                {
                    CategoryToUpdate.IsParentCategory = true;
                }
            }
            catch (Exception ex)
            {

                HelperStoreSqlLog.WriteError(ex, "Error - LegalHunter");
            }
            return View(CategoryToUpdate);
        }
        [HttpPost]
        public IActionResult Category(CategoryViewModel PostObject)
        {
            try
            {
                int CategoryIDtoUpdate;
                if (PostObject.Option == "Edit")
                {
                    CategoryIDtoUpdate = PostObject.CategoryID;
                }
                else
                {
                    Category NewObject = new Category();
                    NewObject.IsActive = true;
                    NewObject.IsDelete = false;
                    db.CategoryTbl.Add(NewObject);
                    db.SaveChanges();
                    CategoryIDtoUpdate = NewObject.CategoryID;
                }
                var CategoryUpdate = db.CategoryTbl.Where(x => x.CategoryID == CategoryIDtoUpdate).FirstOrDefault();
                CategoryUpdate.CategoryName = PostObject.CategoryName;
                if (PostObject.IsParentCategory == true)
                {
                    PostObject.ParentCategoryID = 0;
                }
                CategoryUpdate.ParentCategoryID = PostObject.ParentCategoryID;
                CategoryUpdate.CategoryDesc = PostObject.CategoryDesc;
                db.SaveChanges();
                TempData["AlertStatus"] += Common.GenerateNotification("Category Updated Successfully", "success");
                return RedirectToAction("CategoryList");

            }
            catch (Exception ex)
            {
                TempData["AlertStatus"] += Common.GenerateNotification("Category Update-Error", "danger");
                HelperStoreSqlLog.WriteError(ex, "Error - LegalHunter");
                return RedirectToAction("CategoryList");
            }
            return View();
        }

        [Authorize(Roles = "PlatformOwner")]
        public IActionResult CategoryList()
        {
            return View();
        }


        [AllowAnonymous]
        public string SearchCategory()
        {
            string limit, start, searchKey, orderColumn, orderDir, draw, jsonString, filterType;
            limit = HttpContext.Request.Form["length"].FirstOrDefault();
            start = HttpContext.Request.Form["start"].FirstOrDefault();
            searchKey = HttpContext.Request.Form["search[value]"].FirstOrDefault();
            //  filterType = HttpContext.Request.Form["search[value]"].FirstOrDefault();
            orderColumn = HttpContext.Request.Form["order[0][column]"].FirstOrDefault();
            orderDir = HttpContext.Request.Form["order[0][dir]"].FirstOrDefault();
            draw = HttpContext.Request.Form["draw"].FirstOrDefault();

            using (IDbConnection conn = new SqlConnection(_connectionString))
            {

                var reader = conn.QueryMultiple("SearchCategory", param: new { orderColumn = orderColumn, limit = limit, orderDir = orderDir, start = start, searchKey = searchKey }, commandType: CommandType.StoredProcedure);

                var CategorySearchList = reader.Read<CategorySearchModel>().ToList();

                int count = 0;
                if (CategorySearchList.Count > 0)
                {
                    count = CategorySearchList.FirstOrDefault().TotalCount;
                }
                dynamic newtonresult = new
                {
                    status = "success",
                    draw = Convert.ToInt32(draw == "" ? "0" : draw),
                    recordsTotal = count,
                    recordsFiltered = count,
                    data = CategorySearchList
                };
                jsonString = JsonConvert.SerializeObject(newtonresult);

                return jsonString;
            }

        }

        public class CategorySearchModel : CategoryViewModel
        {
            public int TotalCount { get; set; }
            public string ParentCategoryName { get; set; }
        }

        [Authorize(Roles = "PlatformOwner")]
        public ActionResult categoryStatus(int id, bool Status)
        {
            try
            {


                var CategoryDetails = db.CategoryTbl.Where(x => x.CategoryID == id).FirstOrDefault();

                if (CategoryDetails != null)
                {

                    CategoryDetails.IsActive = Status;
                    db.SaveChanges();
                    TempData["AlertStatus"] += Common.GenerateNotification("Category Status Updated Successfully", "warning");
                    return RedirectToAction("CategoryList");
                }
                else
                {
                    TempData["AlertStatus"] += Common.GenerateNotification("Category Status Update-Error", "danger");
                    return RedirectToAction("CategoryList");
                }
            }
            catch (Exception ex)
            {
                HelperStoreSqlLog.WriteError(ex, "Error - Legal Hunter");
                return RedirectToAction("CategoryList");
            }


        }
        [Authorize(Roles = "PlatformOwner")]
        public IActionResult DeleteCategory(int id)
        {
            try
            {


                var CategoryDetails = db.CategoryTbl.Where(x => x.CategoryID == id).FirstOrDefault();

                if (CategoryDetails != null)
                {
                    
                    CategoryDetails.IsActive = false;
                    CategoryDetails.IsDelete = true;
                    db.SaveChanges();
                    TempData["AlertStatus"] += Common.GenerateNotification("Category Status Deleted Successfully", "warning");
                    return RedirectToAction("CategoryList");
                }
                else
                {
                    TempData["AlertStatus"] += Common.GenerateNotification("Category Status Delete-Error", "danger");
                    return RedirectToAction("CategoryList");
                }
            }
            catch (Exception ex)
            {
                HelperStoreSqlLog.WriteError(ex, "Error - Legal Hunter");
                return RedirectToAction("CategoryList");
            }
        }

        #endregion



        public List<CategoryViewModel> GetCategoryData()
        {
            var GetQueryData = db.CategoryTbl.Where(x =>x.IsActive==true).ToList();
            List<CategoryViewModel> CategoryList = new List<CategoryViewModel>();
            foreach (Category cat in GetQueryData)
            {
                CategoryViewModel NewCategory = new CategoryViewModel();
                NewCategory.CategoryID = cat.CategoryID;
                NewCategory.CategoryName = cat.CategoryName;
                NewCategory.CategoryDesc = cat.CategoryDesc;
                NewCategory.IsActive = cat.IsActive;
                NewCategory.ParentCategoryID = cat.ParentCategoryID;
                NewCategory.FullCategory = cat.CategoryName;

                CategoryList.Add(NewCategory);
            }
            foreach (CategoryViewModel catView in CategoryList.Where(c => c.ParentCategoryID != 0))
            {
                catView.FullCategory = CategoryViewModel.BuildCatStringList(String.Empty, catView, CategoryList, catView.ParentCategoryID);
            }
            var GetQuery = CategoryList;
            return CategoryList;
        }
    }
}
