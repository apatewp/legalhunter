﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Dapper;
using LegalHunter.Data;
using LegalHunter.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using Microsoft.AspNetCore.Http;
using ExcelDataReader;
using OfficeOpenXml;

namespace LegalHunter.Controllers
{
    [Authorize(Roles = "PlatformOwner")]
    public class ExamsController : Controller
    {
        readonly ApplicationDbContext db;
        private readonly IHostingEnvironment _env;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;
        public ExamsController(IHostingEnvironment env, IMapper mapper, ApplicationDbContext Context, UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> SignInManager, IConfiguration configuration)
        {
            _env = env;
            db = Context;
            _userManager = userManager;
            _signInManager = SignInManager;
            _mapper = mapper;
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        public IActionResult Index()
        {
            return View();
        }
        #region Exam
        [Authorize(Roles = "PlatformOwner")]
        public IActionResult Exam(int ExamID, string Option)
        {
            ViewBag.ExamCategoryList = GetCategoryData();
            ExamViewModel Exam = new ExamViewModel();
            try
            {
                if (Option == "Edit")
                {
                    var ExamDetails = db.ExamTbl.Where(x => x.ExamID == ExamID).FirstOrDefault();
                    Exam = _mapper.Map<ExamViewModel>(ExamDetails);
                    Exam.Option = "Edit";
                }
                else
                {
                    Exam.ExamCategoryIDf = 1;

                }
            }
            catch (Exception ex)
            {
                HelperStoreSqlLog.WriteError(ex, "Error - LegalHunter");
            }
            return View(Exam);
        }

        [Authorize(Roles = "PlatformOwner")]
        [HttpPost]
        public async Task<IActionResult> Exam(ExamViewModel PostObject)
        {
            ViewBag.ExamCategoryList = db.CategoryTbl.Where(x => x.IsActive == true && x.IsDelete == false).ToList();
            try
            {
                int ExamIDtoUpdate;
                if (PostObject.Option == "Edit")
                {
                    ExamIDtoUpdate = PostObject.ExamID;
                }
                else
                {
                    Exam NewObject = new Exam();
                    NewObject.ExamCreatedBy = GetUserID();
                    NewObject.ExamCreatedOn = Common.GetCurrentDateTime();
                    NewObject.ExamCreatedByBranchID = 1;
                    NewObject.IsActive = true;
                    NewObject.IsDelete = false;
                    db.ExamTbl.Add(NewObject);
                    db.SaveChanges();
                    ExamIDtoUpdate = NewObject.ExamID;
                }
                var ExamUpdate = db.ExamTbl.Where(x => x.ExamID == ExamIDtoUpdate).FirstOrDefault();
                ExamUpdate.ExamName = PostObject.ExamName;
                ExamUpdate.ExamCategoryIDf = 1;
                ExamUpdate.ExamDuration = PostObject.ExamDuration;
                ExamUpdate.ExamDesc = PostObject.ExamDesc;
                db.SaveChanges();
                TempData["AlertStatus"] += Common.GenerateNotification("Exam Updated Successfully", "success");
                return RedirectToAction("ExamList");

            }
            catch (Exception ex)
            {
                HelperStoreSqlLog.WriteError(ex, "Error - LegalHunter");
                TempData["AlertStatus"] += Common.GenerateNotification("Exam Update-Error", "danger");
                return View(PostObject);
            }
        }


        [Authorize(Roles = "PlatformOwner")]
        public IActionResult ExamList()
        {
            return View();
        }


        [AllowAnonymous]
        public string SearchExam()
        {
            string limit, start, searchKey, orderColumn, orderDir, draw, jsonString, filterType;
            limit = HttpContext.Request.Form["length"].FirstOrDefault();
            start = HttpContext.Request.Form["start"].FirstOrDefault();
            searchKey = HttpContext.Request.Form["search[value]"].FirstOrDefault();
            //  filterType = HttpContext.Request.Form["search[value]"].FirstOrDefault();
            orderColumn = HttpContext.Request.Form["order[0][column]"].FirstOrDefault();
            orderDir = HttpContext.Request.Form["order[0][dir]"].FirstOrDefault();
            draw = HttpContext.Request.Form["draw"].FirstOrDefault();

            using (IDbConnection conn = new SqlConnection(_connectionString))
            {

                var reader = conn.QueryMultiple("SearchExam", param: new { orderColumn = orderColumn, limit = limit, orderDir = orderDir, start = start, searchKey = searchKey }, commandType: CommandType.StoredProcedure);

                var ExamSearchList = reader.Read<ExamSearchModel>().ToList();

                int count = 0;
                if (ExamSearchList.Count > 0)
                {
                    count = ExamSearchList.FirstOrDefault().TotalCount;
                }
                dynamic newtonresult = new
                {
                    status = "success",
                    draw = Convert.ToInt32(draw == "" ? "0" : draw),
                    recordsTotal = count,
                    recordsFiltered = count,
                    data = ExamSearchList
                };
                jsonString = JsonConvert.SerializeObject(newtonresult);

                return jsonString;
            }

        }

        public class ExamSearchModel : ExamViewModel
        {
            public int TotalCount { get; set; }
            public string ExamCategoryName { get; set; }
            public string CreatedOnString { get; set; }

        }

        [Authorize(Roles = "PlatformOwner")]
        public ActionResult examStatus(int id, bool Status)
        {
            try
            {


                var ExamDetails = db.ExamTbl.Where(x => x.ExamID == id).FirstOrDefault();

                if (ExamDetails != null)
                {
                    //db.RawMaterialTbls.Remove(CheckRawMaterial); USED FOR DELETE FROM PROJECT
                    ExamDetails.IsActive = Status;
                    db.SaveChanges();
                    TempData["AlertStatus"] += Common.GenerateNotification("Exam Status Updated Successfully", "warning");
                    return RedirectToAction("ExamList");
                }
                else
                {
                    TempData["AlertStatus"] += Common.GenerateNotification("Exam Status Update-Error", "danger");
                    return RedirectToAction("ExamList");
                }
            }
            catch (Exception ex)
            {
                HelperStoreSqlLog.WriteError(ex, "Error - Legal Hunter");
                return RedirectToAction("ExamList");
            }


        }
        [Authorize(Roles = "PlatformOwner")]
        public IActionResult DeleteExam(int id)
        {
            try
            {


                var ExamDetails = db.ExamTbl.Where(x => x.ExamID == id).FirstOrDefault();

                if (ExamDetails != null)
                {
                    //db.RawMaterialTbls.Remove(CheckRawMaterial); USED FOR DELETE FROM PROJECT
                    ExamDetails.IsActive = false;
                    ExamDetails.IsDelete = true;
                    db.SaveChanges();
                    TempData["AlertStatus"] += Common.GenerateNotification("Exam Status Deleted Successfully", "warning");
                    return RedirectToAction("ExamList");
                }
                else
                {
                    TempData["AlertStatus"] += Common.GenerateNotification("Exam Status Delete-Error", "danger");
                    return RedirectToAction("ExamList");
                }
            }
            catch (Exception ex)
            {
                HelperStoreSqlLog.WriteError(ex, "Error - Legal Hunter");
                return RedirectToAction("ExamList");
            }
        }
        public IActionResult DownloadAnswerKey(int id)
        {
            var ExamAnswerKey = from items in db.ExamQuestionsMapTbl where items.ExamIDf == id select new { QuestionNo = items.ExamQuestionNo, Answer = items.CorrectOption };
            var stream = new MemoryStream();
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (var package = new ExcelPackage(stream))
            {
                var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                workSheet.Cells.LoadFromCollection(ExamAnswerKey, true);
                package.Save();
            }
            stream.Position = 0;
            string excelName = $"AnswerKey-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";

            //return File(stream, "application/octet-stream", excelName);  
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);
        }
        #endregion

        #region Bulk Question Upload
        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 1073741824)]
        public async Task<IActionResult> UploadBulkQuestions(QuestionViewModel PostObject, IFormFile QuestionsListFile, IFormFile QuestionsImageFile)
        {
            var CurrentDate = Common.GetCurrentDateTime();
            var RelativePath = "QuestionImageTempUploads";
            var extractPath = Path.Combine(_env.WebRootPath, RelativePath);

            var filePath = Path.Combine(extractPath, QuestionsListFile.FileName);
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                await QuestionsListFile.CopyToAsync(fileStream);
                fileStream.Close();
            }

            try
            {

                List<QuestionViewModel> questions = new List<QuestionViewModel>();
                // For .net core, the next line requires the NuGet package, 
                // System.Text.Encoding.CodePages
                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

                var streamFile = System.IO.File.Open(filePath, FileMode.Open, FileAccess.Read);

                IExcelDataReader excelReader = null;
                if (filePath.EndsWith(".xls"))
                {
                    excelReader = ExcelReaderFactory.CreateBinaryReader(streamFile);
                }
                if (filePath.EndsWith(".xlsx"))
                {
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(streamFile);
                }
                // var excelReader = ExcelReaderFactory.CreateBinaryReader(streamFile);

                var dataSet = excelReader.AsDataSet(new ExcelDataSetConfiguration
                {
                    ConfigureDataTable = _ => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = true // Use first row is ColumnName here :D
                    }
                });
                streamFile.Close();
                var dataSet2 = dataSet.Tables[0].Rows;

                DataTable dt = dataSet.Tables[0];

                var UploadItemList = (from DataRow x in dataSet2

                                      select new
                                      {

                                          QuestionTitle = x[PostObject.QuestionTitle] == null ? "" : x[PostObject.QuestionTitle].ToString().Trim(),
                                          QuestionDesc = x[PostObject.QuestionDesc] == null ? "" : x[PostObject.QuestionDesc].ToString().Trim(),
                                          CorrectOption = x[PostObject.CorrectOption] == null ? "" : x[PostObject.CorrectOption].ToString().Trim(),
                                          OptionA = x[PostObject.OptionA] == null ? "" : x[PostObject.OptionA].ToString().Trim(),
                                          OptionB = x[PostObject.OptionB] == null ? "" : x[PostObject.OptionB].ToString().Trim(),
                                          OptionC = x[PostObject.OptionC] == null ? "" : x[PostObject.OptionC].ToString().Trim(),
                                          OptionD = x[PostObject.OptionD] == null ? "" : x[PostObject.OptionD].ToString().Trim(),
                                          OptionAImg = x[PostObject.OptionAImg] == null ? "" : x[PostObject.OptionAImg].ToString().Trim(),
                                          OptionBImg = x[PostObject.OptionBImg] == null ? "" : x[PostObject.OptionBImg].ToString().Trim(),
                                          OptionCImg = x[PostObject.OptionCImg] == null ? "" : x[PostObject.OptionCImg].ToString().Trim(),
                                          OptionDImg = x[PostObject.OptionDImg] == null ? "" : x[PostObject.OptionDImg].ToString().Trim(),

                                      }).ToList();




                string[] files = Directory.GetFiles(extractPath);
                foreach (string file in files)
                {
                    System.IO.File.Delete(file);
                }
                using (var stream = QuestionsImageFile.OpenReadStream())
                {
                    ZipArchive archive = new ZipArchive(stream);
                    ZipArchiveExtensions.ExtractToDirectory(archive, extractPath, true);
                }
                var LastExamQuestionNo = db.ExamQuestionsMapTbl.Where(x => x.IsActive == true && x.ExamIDf == PostObject.ExamIDf).OrderByDescending(x => x.ExamQuestionNo).Select(x => x.ExamQuestionNo).FirstOrDefault();
                int ExamQuestionNo = 0;
                if (LastExamQuestionNo != null)
                {
                    ExamQuestionNo = LastExamQuestionNo + 1;
                }
                else
                {
                    ExamQuestionNo = 1;
                }
                int QuestionStartCount = ExamQuestionNo;
                foreach (var Item in UploadItemList)
                {

                    string json = JsonConvert.SerializeObject(Item);

                    if (Item.QuestionTitle != null && Item.QuestionTitle != "")
                    {

                        var CheckExamExists = db.ExamQuestionsMapTbl.Where(x => x.QuestionTitle == Item.QuestionTitle && x.ExamIDf==PostObject.ExamIDf).FirstOrDefault();

                        if (CheckExamExists == null)
                        {
                          
                            ExamQuestionsMap NewQuestion = new ExamQuestionsMap();
                            NewQuestion.QuestionTitle = Item.QuestionTitle;
                            NewQuestion.QuestionDesc = Item.QuestionDesc;
                            NewQuestion.ExamQuestionNo = QuestionStartCount;
                            NewQuestion.ExamIDf = PostObject.ExamIDf;
                            NewQuestion.OptionA = Item.OptionA;
                            NewQuestion.OptionB = Item.OptionB;
                            NewQuestion.OptionC = Item.OptionC;
                            NewQuestion.OptionD = Item.OptionD;
                            NewQuestion.CorrectOption = Item.CorrectOption;
                            var GetImageFileName = "";
                            int idx = QuestionsImageFile.FileName.LastIndexOf('.');

                            if (idx != -1)
                            {
                                GetImageFileName = QuestionsImageFile.FileName.Substring(0, idx);
                            }
                            if (System.IO.File.Exists(extractPath + @"\" + GetImageFileName + @"\" + Item.OptionAImg))
                            {
                                var SourcePath = extractPath + @"\" + GetImageFileName + @"\" + Item.OptionAImg;
                                var RelativePathExam = "uploads/QuestionOptions/" + CurrentDate.Year + "/" + CurrentDate.Month;
                                var PathWithFolderName = Path.Combine(_env.WebRootPath, RelativePathExam);
                                var OptionAImgFileName = "ExamQuestionID-" + PostObject.ExamQuestionID + "-OptionAImg-id" + Common.generateID() + ".jpeg";

                                if (!Directory.Exists(PathWithFolderName))
                                    Directory.CreateDirectory(PathWithFolderName);

                                var filePathProduct = Path.Combine(PathWithFolderName, OptionAImgFileName);
                                System.IO.File.Copy(SourcePath, filePathProduct, true);
                                NewQuestion.OptionAImg = RelativePathExam + "/" + OptionAImgFileName;
                                NewQuestion.IsOptionANeedImg = true;
                            }
                            else
                            {
                                NewQuestion.IsOptionANeedImg = false;
                            }
                            if (System.IO.File.Exists(extractPath + @"\" + GetImageFileName + @"\" + Item.OptionBImg))
                            {
                                var SourcePath = extractPath + @"\" + GetImageFileName + @"\" + Item.OptionBImg;
                                var RelativePathExam = "uploads/QuestionOptions/" + CurrentDate.Year + "/" + CurrentDate.Month;
                                var PathWithFolderName = Path.Combine(_env.WebRootPath, RelativePathExam);
                                var OptionBImgFileName = "ExamQuestionID-" + PostObject.ExamQuestionID + "-OptionBImg-id" + Common.generateID() + ".jpeg";

                                if (!Directory.Exists(PathWithFolderName))
                                    Directory.CreateDirectory(PathWithFolderName);

                                var filePathProduct = Path.Combine(PathWithFolderName, OptionBImgFileName);
                                System.IO.File.Copy(SourcePath, filePathProduct, true);
                                NewQuestion.OptionBImg = RelativePathExam + "/" + OptionBImgFileName;
                                NewQuestion.IsOptionBNeedImg = true;
                            }
                            else
                            {
                                NewQuestion.IsOptionBNeedImg = false;
                            }
                            if (System.IO.File.Exists(extractPath + @"\" + GetImageFileName + @"\" + Item.OptionCImg))
                            {
                                var SourcePath = extractPath + @"\" + GetImageFileName + @"\" + Item.OptionCImg;
                                var RelativePathExam = "uploads/QuestionOptions/" + CurrentDate.Year + "/" + CurrentDate.Month;
                                var PathWithFolderName = Path.Combine(_env.WebRootPath, RelativePathExam);
                                var OptionCImgFileName = "ExamQuestionID-" + PostObject.ExamQuestionID + "-OptionCImg-id" + Common.generateID() + ".jpeg";

                                if (!Directory.Exists(PathWithFolderName))
                                    Directory.CreateDirectory(PathWithFolderName);

                                var filePathProduct = Path.Combine(PathWithFolderName, OptionCImgFileName);
                                System.IO.File.Copy(SourcePath, filePathProduct, true);
                                NewQuestion.OptionCImg = RelativePathExam + "/" + OptionCImgFileName;
                                NewQuestion.IsOptionCNeedImg = true;
                            }
                            else
                            {
                                NewQuestion.IsOptionCNeedImg = false;
                            }

                            if (System.IO.File.Exists(extractPath + @"\" + GetImageFileName + @"\" + Item.OptionDImg))
                            {
                                var SourcePath = extractPath + @"\" + GetImageFileName + @"\" + Item.OptionDImg;
                                var RelativePathExam = "uploads/QuestionOptions/" + CurrentDate.Year + "/" + CurrentDate.Month;
                                var PathWithFolderName = Path.Combine(_env.WebRootPath, RelativePathExam);
                                var OptionDImgFileName = "ExamQuestionID-" + PostObject.ExamQuestionID + "-OptionDImg-id" + Common.generateID() + ".jpeg";

                                if (!Directory.Exists(PathWithFolderName))
                                    Directory.CreateDirectory(PathWithFolderName);

                                var filePathProduct = Path.Combine(PathWithFolderName, OptionDImgFileName);
                                System.IO.File.Copy(SourcePath, filePathProduct, true);
                                NewQuestion.IsOptionDNeedImg = true;
                            }
                            else
                            {
                                NewQuestion.IsOptionDNeedImg = false;
                            }

                            NewQuestion.IsActive = true;
                            NewQuestion.IsDelete = false;
                            db.ExamQuestionsMapTbl.Add(NewQuestion);

                            QuestionStartCount++;


                        }

                    }
                }
                db.SaveChanges();

                TempData["AlertStatus"] += Common.GenerateNotification("Bulk Question Uploaded Successfully", "success");
                return RedirectToAction("Exam", new { ExamID = PostObject.ExamIDf, Option = "Edit" });
            }
            catch (Exception ex)
            {
                //HelperStoreSqlLog.WriteError(ex, "Error - Tagprinting");
                TempData["AlertStatus"] += Common.GenerateNotification("Bulk Question Uploaded-Error,Please Check whether you have mapped all fields", "danger");
                return RedirectToAction("Exam", new { ExamID = PostObject.ExamIDf, Option = "Edit" });
            }

        }


        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 1073741824)]
        public async Task<IActionResult> LoadExcelColumHeader(IFormFile QuestionsListFile)
        {

            var CurrentDate = Common.GetCurrentDateTime();
            var RelativePath = "QuestionImageTempUploads";
            var extractPath = Path.Combine(_env.WebRootPath, RelativePath);
            var filePath = Path.Combine(extractPath, QuestionsListFile.FileName);
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                await QuestionsListFile.CopyToAsync(fileStream);
                fileStream.Close();
            }
            // For .net core, the next line requires the NuGet package, 
            // System.Text.Encoding.CodePages
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            var streamFile = System.IO.File.Open(filePath, FileMode.Open, FileAccess.Read);

            IExcelDataReader excelReader = null;
            if (filePath.EndsWith(".xls"))
            {
                excelReader = ExcelReaderFactory.CreateBinaryReader(streamFile);
            }
            if (filePath.EndsWith(".xlsx"))
            {
                excelReader = ExcelReaderFactory.CreateOpenXmlReader(streamFile);
            }
            // var excelReader = ExcelReaderFactory.CreateBinaryReader(streamFile);

            var dataSet = excelReader.AsDataSet(new ExcelDataSetConfiguration
            {
                ConfigureDataTable = _ => new ExcelDataTableConfiguration
                {
                    UseHeaderRow = true // Use first row is ColumnName here :D
                }
            });
            streamFile.Close();
            var dataSet2 = dataSet.Tables[0].Rows;
            List<string> ColumnNameList = new List<string>();
            foreach (DataColumn c in dataSet.Tables[0].Columns)
            {
                ColumnNameList.Add(c.ColumnName);
            }
            return Json(ColumnNameList);
        }

        #endregion
        public string GetUserID()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            return userId;
        }
        public List<CategoryViewModel> GetCategoryData()
        {
            var GetQueryData = db.CategoryTbl.Where(x => x.IsActive == true).ToList();
            List<CategoryViewModel> CategoryList = new List<CategoryViewModel>();
            foreach (Category cat in GetQueryData)
            {
                CategoryViewModel NewCategory = new CategoryViewModel();
                NewCategory.CategoryID = cat.CategoryID;
                NewCategory.CategoryName = cat.CategoryName;
                NewCategory.CategoryDesc = cat.CategoryDesc;
                NewCategory.IsActive = cat.IsActive;
                NewCategory.ParentCategoryID = cat.ParentCategoryID;
                NewCategory.FullCategory = cat.CategoryName;

                CategoryList.Add(NewCategory);
            }
            foreach (CategoryViewModel catView in CategoryList.Where(c => c.ParentCategoryID != 0))
            {
                catView.FullCategory = CategoryViewModel.BuildCatStringList(String.Empty, catView, CategoryList, catView.ParentCategoryID);
            }
            var GetQuery = CategoryList;
            return CategoryList;
        }

        #region Helpers
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();

                }

            }

            base.Dispose(disposing);
        }

        #endregion

    }

    public static class ZipArchiveExtensions
    {
        public static void ExtractToDirectory(this ZipArchive archive, string destinationDirectoryName, bool overwrite)
        {
            if (!overwrite)
            {
                archive.ExtractToDirectory(destinationDirectoryName);
                return;
            }
            foreach (ZipArchiveEntry file in archive.Entries)
            {
                string completeFileName = Path.Combine(destinationDirectoryName, file.FullName);
                string directory = Path.GetDirectoryName(completeFileName);

                if (!Directory.Exists(directory))
                    Directory.CreateDirectory(directory);

                if (file.Name != "")
                    file.ExtractToFile(completeFileName, true);
            }
        }

        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
    }
}
