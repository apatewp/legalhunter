﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Routing;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LegalHunter.Data;



namespace LegalHunter
{
   // private readonly ApplicationDbContext db;

    public static class extensionmethods
    {
        

    }
    public static class UrlHelperExtensions
    {
        public static string ContentVersioned(this Microsoft.AspNetCore.Mvc.IUrlHelper self, string contentPath)
        {
            string versionedContentPath = contentPath + "?v=2" + Assembly.GetAssembly(typeof(UrlHelperExtensions)).GetName().Version.ToString();
            return self.Content(versionedContentPath);
        }
    }
    public static class StringExt
    {
        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }
    }

 

    public static class Common
    {
        public static IQueryable<TEntity> OrderBy<TEntity>(this IQueryable<TEntity> source, string orderByProperty, bool desc)
        {
            string command = desc ? "OrderByDescending" : "OrderBy";
            var type = typeof(TEntity);
            var property = type.GetProperty(orderByProperty);
            var parameter = Expression.Parameter(type, "p");
            var propertyAccess = Expression.MakeMemberAccess(parameter, property);
            var orderByExpression = Expression.Lambda(propertyAccess, parameter);
            var resultExpression = Expression.Call(typeof(Queryable), command, new Type[] { type, property.PropertyType },
                                          source.Expression, Expression.Quote(orderByExpression));
            return source.Provider.CreateQuery<TEntity>(resultExpression);
        }



   
        public static async Task SendHTMLMail(string mailSubject, string sentTo, string htmlMailBody)
        {
              var fromAddress = new MailAddress("fivepercentllc@gmail.com", "Search");
                const string fromPassword = "FivePercentLLC123@";

                var toAddress = new MailAddress(sentTo);
                string body = htmlMailBody;
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                    Timeout = 20000
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = mailSubject,
                    Body = body,
                    IsBodyHtml = true
                })
                {
                    await smtp.SendMailAsync(message);
                }
           
        }


        public static int DivideRoundingUp(int x, int y)
        {
            // TODO: Define behaviour for negative numbers
            int remainder;
            int quotient = Math.DivRem(x, y, out remainder);
            return remainder == 0 ? quotient : quotient + 1;
        }
        public static double DecimalRoundUp(double input, int places)
        {
            double multiplier = Math.Pow(10, Convert.ToDouble(places));
            return Math.Ceiling(input * multiplier) / multiplier;
        }

        //public static async Task GetCurrentUserDetails(string UserID)
        //{


        //    //ApplicationDbContext db = new ApplicationDbContext();
        //    //var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
        //    //  var UserDetails = db.Users.Where(x => x.Id == UserID).FirstOrDefault();
        //    var user = await UserManager.GetUserAsync(UserID);
        //    return user;
        //}



        //public static void ListToExcel<T>(List<T> query, string FileName)
        //{
        //    using (ExcelPackage pck = new ExcelPackage())
        //    {
        //        //Create the worksheet
        //        ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Result");

        //        //get our column headings
        //        var t = typeof(T);
        //        var Headings = t.GetProperties();
        //        for (int i = 0; i < Headings.Count(); i++)
        //        {

        //            ws.Cells[1, i + 1].Value = Headings[i].Name;
        //        }

        //        //populate our Data
        //        if (query.Count() > 0)
        //        {
        //            ws.Cells["A2"].LoadFromCollection(query);
        //        }

        //        //Format the header
        //        using (ExcelRange rng = ws.Cells["A1:BZ1"])
        //        {
        //            rng.Style.Font.Bold = true;
        //            rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
        //            rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
        //            rng.Style.Font.Color.SetColor(Color.White);
        //        }

        //        //Write it back to the client
        //        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;  filename=" + FileName + ".xlsx");
        //        HttpContext.Current.Response.BinaryWrite(pck.GetAsByteArray());
        //        HttpContext.Current.Response.End();
        //    }
        //}
        //public static string SaveImageFolder(HttpPostedFileBase Image, string FolderName, int? ID, string SpecificName)
        //{
        //    var Img = Image;
        //    if (Img != null && Img.ContentLength > 0)
        //    {
        //        FileInfo fi = new FileInfo(Img.FileName);
        //        string ext = fi.Extension;
        //        var fileName = Path.GetFileName(Img.FileName);
        //        var ProductCategoryName = Common.GetURLFriendlyString(SpecificName);
        //        fileName = "Image-" + ID + '-' + ProductCategoryName + ext;
        //        var path = Path.Combine(HttpContext.Current.Server.MapPath(FolderName), fileName);
        //        Img.SaveAs(path);

        //        return fileName;
        //    }
        //    else
        //    {
        //        return "";
        //    }
        //}

        //public static void PushNotification(int? NewsID, string ImageLink, string Heading)
        //{
        //    NewsOnlineDevelopmentEntities db = new NewsOnlineDevelopmentEntities();


        //    var requests = WebRequest.Create("https://onesignal.com/api/v1/notifications") as HttpWebRequest;
        //    // var LoginDetails = GetLogInDetails(User.Identity.GetUserId());


        //    int PublicationID = Convert.ToInt32(db.NewsPublicationMapTbls.ToList().Where(x => x.NewsID == NewsID).Select(x => x.PublicationID).FirstOrDefault());
        //    var UserIDList = db.FavoriteTbls.Where(x => x.PublicationID == PublicationID).Select(x => x.FavoritedBy).ToList();
        //    string UsersList = "";
        //    if (UserIDList != null)
        //    {
        //        UserIDList = UserIDList.Distinct().ToList();
        //        UsersList = String.Join("\",\"", UserIDList);
        //        // UsersList = "f45114a5-4102-4ba8-83fa-7b0288d85f35";
        //    }
        //    requests.KeepAlive = true;
        //    requests.Method = "POST";
        //    requests.ContentType = "application/json; charset=utf-8";

        //    requests.Headers.Add("authorization", "Basic NTA5OWI2ZmUtYzhhNi00ZDExLWE0ZDUtNjg4M2U4ODkyMDA0");

        //    byte[] byteArray = Encoding.UTF8.GetBytes("{"
        //                                            + "\"app_id\": \"3abfb023-b02f-464a-98ae-bd9a56238558\","
        //                                          + "\"headings\": {\"en\": \" Latest News  \"}," //notification title 
        //                                            + "\"url\" : \"https://patram.com/News/readfullnews?NewsID=" + NewsID + "\"," //redirection link
        //                                            + "\"chrome_web_image\" : \"https://patram.com/Uploads/Thumbnail/" + ImageLink + "\","  //content Image
        //                                                                                                                                    //    + "\"chrome_web_image\" : \"https://media.wired.com/photos/5c1ae77ae91b067f6d57dec0/master/pass/Comparison-City-MAIN-ART.jpg\","  //content Image
        //                                            + "\"chrome_web_badge\" : \"https://patram.com/Images/pticon.png\","  //Badge Image
        //                                            + "\"contents\": {\"en\": \"" + Heading + "\"}," // content  details
        //                                            + "\"include_external_user_ids\": [\"" + UsersList + "\"]}");

        //    // + "\"included_segments\": [\"Active Users\"]}"); // user types
        //    // + "\"external_user_id\": [\"f45114a5-4102-4ba8-83fa-7b0288d85f35\"]}"); // user types
        //    string responseContent = null;

        //    try
        //    {
        //        using (var writer = requests.GetRequestStream())
        //        {
        //            writer.Write(byteArray, 0, byteArray.Length);
        //        }

        //        using (var response = requests.GetResponse() as HttpWebResponse)
        //        {
        //            using (var reader = new StreamReader(response.GetResponseStream()))
        //            {
        //                responseContent = reader.ReadToEnd();
        //            }
        //        }
        //    }
        //    catch (WebException ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(ex.Message);
        //        System.Diagnostics.Debug.WriteLine(new StreamReader(ex.Response.GetResponseStream()).ReadToEnd());
        //    }

        //    System.Diagnostics.Debug.WriteLine(responseContent);

        //}
        public static string GetBytesReadable(long i)
        {
            // Get absolute value
            long absolute_i = (i < 0 ? -i : i);
            // Determine the suffix and readable value
            string suffix;
            double readable;
            if (absolute_i >= 0x1000000000000000) // Exabyte
            {
                suffix = "EB";
                readable = (i >> 50);
            }
            else if (absolute_i >= 0x4000000000000) // Petabyte
            {
                suffix = "PB";
                readable = (i >> 40);
            }
            else if (absolute_i >= 0x10000000000) // Terabyte
            {
                suffix = "TB";
                readable = (i >> 30);
            }
            else if (absolute_i >= 0x40000000) // Gigabyte
            {
                suffix = "GB";
                readable = (i >> 20);
            }
            else if (absolute_i >= 0x100000) // Megabyte
            {
                suffix = "MB";
                readable = (i >> 10);
            }
            else if (absolute_i >= 0x400) // Kilobyte
            {
                suffix = "KB";
                readable = i;
            }
            else
            {
                return i.ToString("0 B"); // Byte
            }
            // Divide by 1024 to get fractional value
            readable = (readable / 1024);
            // Return formatted number with suffix
            return readable.ToString("0.##") +" "+ suffix;
        }

        public static long DirSize(DirectoryInfo d)
        {
            long size = 0;
            // Add file sizes.
            FileInfo[] fis = d.GetFiles();
            foreach (FileInfo fi in fis)
            {
                size += fi.Length;
            }
            // Add subdirectory sizes.
            DirectoryInfo[] dis = d.GetDirectories();
            foreach (DirectoryInfo di in dis)
            {
                size += DirSize(di);
            }
            return size;
        }
        public static string GenerateSmallUniqueCode()
        {
            var chars1 = "abcdefghijklmnopqrstuvwxyz1234567890";
            var stringChars1 = new char[5];
            var random1 = new Random();

            for (int i = 0; i < stringChars1.Length; i++)
            {
                stringChars1[i] = chars1[random1.Next(chars1.Length)];
            }

            var str = new String(stringChars1);
            return str;
        }

        //public static string RemoveUnwantedHTMLTags(string data)
        //{
        //    if (string.IsNullOrEmpty(data)) return string.Empty;

        //    var document = new HtmlDocument();
        //    document.LoadHtml(data);

        //    var acceptableTags = new String[] { }; //"strong", "em", "u"

        //    var nodes = new Queue<HtmlNode>(document.DocumentNode.SelectNodes("./*|./text()"));
        //    while (nodes.Count > 0)
        //    {
        //        var node = nodes.Dequeue();
        //        var parentNode = node.ParentNode;

        //        if (!acceptableTags.Contains(node.Name) && node.Name != "#text")
        //        {
        //            var childNodes = node.SelectNodes("./*|./text()");

        //            if (childNodes != null)
        //            {
        //                foreach (var child in childNodes)
        //                {
        //                    nodes.Enqueue(child);
        //                    parentNode.InsertBefore(child, node);
        //                }
        //            }

        //            parentNode.RemoveChild(node);

        //        }
        //    }

        //    return document.DocumentNode.InnerHtml;
        //}

        public static void SendMail(MailMessage Mail)
        {
            /// Command line argument must the the SMTP host.
            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential("testman4techxora@gmail.com", "techxora123456");

            Mail.From = new MailAddress("testman4techxora@gmail.com");
            Mail.BodyEncoding = UTF8Encoding.UTF8;
            Mail.IsBodyHtml = true;
            Mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            client.Send(Mail);
        }

        public static string GenerateLicenseKey(string productIdentifier)
        {
            return FormatLicenseKey(GetMd5Sum(productIdentifier));
        }

        public static string GetMd5Sum(string productIdentifier)
        {
            System.Text.Encoder enc = System.Text.Encoding.Unicode.GetEncoder();
            byte[] unicodeText = new byte[productIdentifier.Length * 2];
            enc.GetBytes(productIdentifier.ToCharArray(), 0, productIdentifier.Length, unicodeText, 0, true);
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] result = md5.ComputeHash(unicodeText);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                sb.Append(result[i].ToString("X2"));
            }
            return sb.ToString();
        }

        public static string FormatLicenseKey(string productIdentifier)
        {
            productIdentifier = productIdentifier.Substring(0, 28).ToUpper();
            char[] serialArray = productIdentifier.ToCharArray();
            StringBuilder licenseKey = new StringBuilder();

            int j = 0;
            for (int i = 0; i < 28; i++)
            {
                for (j = i; j < 4 + i; j++)
                {
                    licenseKey.Append(serialArray[j]);
                }
                if (j == 28)
                {
                    break;
                }
                else
                {
                    i = (j) - 1;
                    licenseKey.Append("-");
                }
            }
            return licenseKey.ToString();
        }



        public static string EncryptBase64(string StringToEncrypt)
        {
            byte[] buffer = (new UnicodeEncoding()).GetBytes(StringToEncrypt);
            return System.Convert.ToBase64String(buffer);
        }

        //private static byte[] keyx = new byte[8] { 1, 2, 3, 4, 5, 6, 7, 8 };
        private static byte[] iv = new byte[8] { 1, 5, 1, 8, 8, 6, 4, 1 };


        public static string EncryptText(string encryptString, string EncryptionKey)
        {

            //string EncryptionKey = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            byte[] clearBytes = Encoding.Unicode.GetBytes(encryptString);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    encryptString = Convert.ToBase64String(ms.ToArray());
                }
            }
            return encryptString;

        }

        public static string DecryptText(string cipherText, string EncryptionKey)
        {
            //string EncryptionKey = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;

        }



        public static string ArrayToStringToUseInsideInClause(int?[] List)
        {
            if (List == null)
            {
                return "";
            }

            var inClause = String.Join(",", List.Select(x => x.ToString()).ToArray());

            return inClause;

            //StringBuilder sb = new StringBuilder();
            //foreach (var li in List)
            //{
            //    // IN clause
            //    sb.Append("'"+li.ToString() + "',");
            //}

            //string Output = sb.ToString();
            //Output = Output.TrimEnd(',');

            //return Output;
        }

        public static bool In<T>(this T source, params T[] list)
        {
            return list.Contains(source);
        }

        public static object CommonClass { get; internal set; }

        public static string generateID()
        {
            return Guid.NewGuid().ToString("N");
        }

        //public static string GetConnectionString()
        //{
        //    // Put the name the Sqlconnection from WebConfig..

        //    var Connection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        //    return Connection;


        //    //return @"data source=mi3-wsq2.a2hosting.com;initial catalog=techxora_PointOfSale;persist security info=True;user id=PointOfSale;password=TechxoraPOS1;MultipleActiveResultSets=True;";

        //    // return @"data source=LAPTOP-8MIN2DB3\SA;initial catalog=techxora_PointOfSale;persist security info=True;user id=sa;password=sa.;MultipleActiveResultSets=True;";
        //    // return @"data source=T1-08\SA2;initial catalog=techxora_PointOfSale;persist security info=True;user id=sa;password=sa.;MultipleActiveResultSets=True;";

        //}



        //public static LoginInfoModel GetCurrentUserDetails(string UserID)
        //{

        //    string ConnectionString = Common.GetConnectionString();
        //    using (IDbConnection conn = new SqlConnection(ConnectionString))
        //    {
        //        var reader = conn.QueryMultiple("LogInInfo", param: new { UserID = UserID }, commandType: CommandType.StoredProcedure);

        //        var LoginInfo = reader.Read<LoginInfoModel>().FirstOrDefault();

        //        return LoginInfo;
        //    }

        //}





        public static string GenerateNotification(string Message, string Type)
        {
            var color = "";
            if (Type=="danger")
            {
                color = "linear-gradient(to right,#d20000,#ff0000)";

            }else if (Type=="warning")
            {
                color = "linear-gradient(to right,#ecb405,#ecb405)";
            }
            else if (Type == "success")
            {
                color = "linear-gradient(to right,#007429,#00c559)";
            }
            else
            {
                color = "#9c9f9d";
            }

            return string.Format("Notification('{0}','{1}');", Message,color);

          //  return "Notification("+Message+","+color+")";
            //return "noty({" +
            //            "text: \"" + Message + "\"," +
            //            "type: \"" + Type + "\", layout: \"bottomCenter\", timeout: 5000," +
            //            "theme: 'relax'," +
            //            "animation: {" +
            //                "open: 'animated bounceInRight'," +
            //                "close: 'animated bounceOutRight'," +
            //                "easing: 'swing'," +
            //                "speed: 500" +
            //            "}" +
            //        "});";

            //return @"$.notify({message: '" + Message + "'}, {allow_dismiss: true,delay: 10000,type: '" + Type + "',placement: {from: 'bottom',align: 'right'}});";

        }


        public static string ConvertNumbertoWords(decimal? val)
        {
            decimal FinalValue = 0;
            decimal.TryParse(val.ToString(), out FinalValue);

            long number = (long)FinalValue;

            if (number == 0) return "ZERO";
            if (number < 0) return "minus " + ConvertNumbertoWords(Math.Abs(number));
            string words = "";
            if ((number / 1000000) > 0)
            {
                words += ConvertNumbertoWords(number / 100000) + " LAKH ";
                number %= 1000000;
            }
            if ((number / 1000) > 0)
            {
                words += ConvertNumbertoWords(number / 1000) + " THOUSAND ";
                number %= 1000;
            }
            if ((number / 100) > 0)
            {
                words += ConvertNumbertoWords(number / 100) + " HUNDRED ";
                number %= 100;
            }
            //if ((number / 10) > 0)  
            //{  
            // words += ConvertNumbertoWords(number / 10) + " RUPEES ";  
            // number %= 10;  
            //}  
            if (number > 0)
            {
                if (words != "") words += "AND ";
                var unitsMap = new[]
                {
                    "ZERO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN"
                };
                var tensMap = new[]
                {
                    "ZERO", "TEN", "TWENTY", "THIRTY", "FORTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY"
                };
                if (number < 20) words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0) words += " " + unitsMap[number % 10];
                }
            }
            return words;
        }

        public static int ConvertNumberToAltestZero(int? value)
        {
            if (value == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(value);
            }
        }

        public static decimal ConvertToDecial(decimal? value)
        {
            if (value == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToDecimal(value);
            }
        }

        public static double ConvertToDouble(decimal? value)
        {
            if (value == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToDouble(value);
            }
        }

        public static decimal ConvertToInt(int? value)
        {
            if (value == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(value);
            }
        }

        public static string DecimalToPrint(decimal? value)
        {
            if (value == null)
            {
                return "";
            }
            else if (value == 0)
            {
                return "";
            }
            else
            {
                return value.ToString().Replace(".00", "");
            }
        }


        public static string IntToPrint(int? value)
        {
            if (value == null)
            {
                return "";
            }
            else if (value == 0)
            {
                return "";
            }
            else
            {
                return value.ToString();
            }
        }

        public static DateTime GetCurrentDateTime()
        {
            DateTime todaysDate = DateTime.Now;
            var gmTime = todaysDate.ToUniversalTime();
            var indianTimeZone = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            var gmTimeConverted = TimeZoneInfo.ConvertTime(gmTime, indianTimeZone);
            DateTime ResultDateTime = Convert.ToDateTime(gmTimeConverted);
            return ResultDateTime;
        }



        public static string CompareDateTime(DateTime FromDate)
        {

            var CurrentTime = DateTime.Now.ToString("HH");
            var CurrentMin = DateTime.Now.ToString("mm");
            int TimeHrdifferent = 0;
            int TimeMindifferent = 0;
            var TodayDate = DateTime.Now.ToString("M/d/yyyy");

            var NewsHr = FromDate.ToString("HH");
            var NewsMin = FromDate.ToString("mm");
            var NewsDate = FromDate.ToString("M/d/yyyy");
            DateTime NewsDATE = FromDate;


            if (NewsDate == TodayDate)
            {

                TimeHrdifferent = Convert.ToInt32(CurrentTime) - Convert.ToInt32(NewsHr);

                TimeMindifferent = Convert.ToInt16(CurrentMin) - Convert.ToInt16(NewsMin);

                if (TimeHrdifferent < 1)
                {
                    if (TimeMindifferent < 1)
                    {
                        return "just now";
                    }
                    else
                    {
                        return TimeMindifferent + " minutes ago";
                    }
                }
                else
                {

                    return TimeHrdifferent + " hours ago";

                }
            }
            else
            {
                return NewsDATE.ToString("MMMM dd, yyyy");
            }

            return "";

        }


        public static string GenerateOTP()
        {
            //string alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            //string small_alphabets = "abcdefghijklmnopqrstuvwxyz";
            string numbers = "1234567890";

            string characters = numbers;
            // if (rbType.SelectedItem.Value == "1")
            // {
            //     characters += alphabets + small_alphabets + numbers;
            //  }
            int length = 6;
            string otp = string.Empty;
            for (int i = 0; i < length; i++)
            {
                string character = string.Empty;
                do
                {
                    int index = new Random().Next(0, characters.Length);
                    character = characters.ToCharArray()[index].ToString();
                } while (otp.IndexOf(character) != -1);
                otp += character;
            }
            return otp;
        }

        public static string GetURLFriendlyString(string Value)
        {
            return Regex.Replace(Value, @"[^A-Za-z0-9_\~]+", "-");
        }
        public static string FormatJobCode(int Number)
        {
            var Code = string.Format("{0:0000}", Number);
            return Code;
        }

        public static string GetFormatCode(int ID)
        {
            var Code = string.Format("{0:000}", ID);
            return Code;
        }
        public static string GetFormatCode2(int ID)
        {
            var Code = string.Format("{0:00}", ID);
            return Code;
        }
        public static string GetFormatCode4(int ID)
        {
            var Code = string.Format("{0:0000}", ID);
            return Code;
        }
        public static string GetSerialFormatCode(int ID)
        {
            var Code = string.Format("{0:00000}", ID);
            return Code;
        }
        public static string GetMonthFormatCode(int ID)
        {
            var Code = string.Format("{0:00}", ID);
            return Code;
        }
        public static int Get2LetterYear(int fourLetterYear)
        {
            return Convert.ToInt32(fourLetterYear.ToString().Substring(2, 2));
        }
        public static void MakeFolderWritable(string Folder)
        {
            if (IsFolderReadOnly(Folder))
            {
                System.IO.DirectoryInfo oDir = new System.IO.DirectoryInfo(Folder);
                oDir.Attributes = oDir.Attributes & ~System.IO.FileAttributes.ReadOnly;
            }
        }
        public static bool IsFolderReadOnly(string Folder)
        {
            System.IO.DirectoryInfo oDir = new System.IO.DirectoryInfo(Folder);
            return ((oDir.Attributes & System.IO.FileAttributes.ReadOnly) > 0);
        }
  

    }
}