﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LegalHunter.Migrations
{
    public partial class ChangeQuestionTbl2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsOptionENeedImg",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.DropColumn(
                name: "OptionE",
                table: "ExamQuestionsMapTbl");

            migrationBuilder.DropColumn(
                name: "OptionEImg",
                table: "ExamQuestionsMapTbl");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsOptionENeedImg",
                table: "ExamQuestionsMapTbl",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "OptionE",
                table: "ExamQuestionsMapTbl",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OptionEImg",
                table: "ExamQuestionsMapTbl",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
