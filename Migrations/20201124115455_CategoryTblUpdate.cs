﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LegalHunter.Migrations
{
    public partial class CategoryTblUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ParentCategoryID",
                table: "CategoryTbl",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ParentCategoryID",
                table: "CategoryTbl");
        }
    }
}
