﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LegalHunter.Migrations
{
    public partial class ExamQuestionNo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ExamQuestionNo",
                table: "ExamQuestionsMapTbl",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExamQuestionNo",
                table: "ExamQuestionsMapTbl");
        }
    }
}
