﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LegalHunter.Data;
using LegalHunter.Models;

namespace LegalHunter.AutoMapper
{
    public class AutomapperProfile:Profile
    {
        public AutomapperProfile()
        {
           CreateMap<Exam, ExamViewModel>();
           CreateMap<ExamQuestionsMap, QuestionViewModel>();
            CreateMap<Category, CategoryViewModel>();
        }
    }
}
