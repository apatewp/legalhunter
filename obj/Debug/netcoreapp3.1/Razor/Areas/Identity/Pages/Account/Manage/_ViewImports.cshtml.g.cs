#pragma checksum "G:\legalHunter\Areas\Identity\Pages\Account\Manage\_ViewImports.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "807afb194b3354e8acb1e4d13ae759a856eb1bda"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Identity_Pages_Account_Manage__ViewImports), @"mvc.1.0.view", @"/Areas/Identity/Pages/Account/Manage/_ViewImports.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "G:\legalHunter\Areas\Identity\Pages\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "G:\legalHunter\Areas\Identity\Pages\_ViewImports.cshtml"
using LegalHunter.Areas.Identity;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "G:\legalHunter\Areas\Identity\Pages\_ViewImports.cshtml"
using LegalHunter.Areas.Identity.Pages;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "G:\legalHunter\Areas\Identity\Pages\_ViewImports.cshtml"
using LegalHunter.Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "G:\legalHunter\Areas\Identity\Pages\Account\_ViewImports.cshtml"
using LegalHunter.Areas.Identity.Pages.Account;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "G:\legalHunter\Areas\Identity\Pages\Account\Manage\_ViewImports.cshtml"
using LegalHunter.Areas.Identity.Pages.Account.Manage;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"807afb194b3354e8acb1e4d13ae759a856eb1bda", @"/Areas/Identity/Pages/Account/Manage/_ViewImports.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e3754fe4f3add1001379748b24381763d81d6a59", @"/Areas/Identity/Pages/_ViewImports.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b787fe0de1475fe0d990f8010f9f75db43f6ea45", @"/Areas/Identity/Pages/Account/_ViewImports.cshtml")]
    public class Areas_Identity_Pages_Account_Manage__ViewImports : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
